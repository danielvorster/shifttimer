from playsound import playsound
import configparser
import os.path
import schedule
import time
import os

WINDOWS_STARTUP_LOC = f'{os.getenv("APPDATA")}\Microsoft\Windows\Start Menu\Programs\Startup'
print(WINDOWS_STARTUP_LOC)

CONFIG_FILE_NAME = f'config.txt'
print(CONFIG_FILE_NAME)
DAYS = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']


# Gen this exec path
# Gen python venv path
# Install script - delete, create venv
# Copy BAT file to startup folder
# Exec program


class ShiftTimer():

    def __init__(self) -> None:
            print('Loading config...')
            self.loadConfig()
            print('Loaded!')
            print('Populating scheduler...')
            self.loadTimes()
            print('Done! Init complete.')
            pass


    def loadConfig(self):
        self.config = configparser.ConfigParser()

        if not os.path.isfile(CONFIG_FILE_NAME):
            for day in DAYS:
                self.config[day] = {
                    'times': '<times>'}
                    
            with open(CONFIG_FILE_NAME, 'w') as configfile:
                self.config.write(configfile)
            print('Please configure config file')
            exit()
        else:
            self.config.read(CONFIG_FILE_NAME)
            if self.config[DAYS[0]]['times'] == '<times>':
                print("Improperly configured!")
                exit()

    def loadTimes(self):
        for day in DAYS:
            self.configureSchedule(day, self.config[day]['times'].split(','))
        pass
    
    def ring(self):
        playsound('sound.mp3')

    def pr(self):
        print('yoyo')

    def configureSchedule(self, day, config):
        for time in config:
            print('Configuring time: ' + day + ' : ' + time)
            eval("schedule.every()."+day).at(time).do(self.ring)


def main():
    s = ShiftTimer()
    print('Running...')
    while True:
        schedule.run_pending()
        time.sleep(1)

if __name__ == '__main__':
    main()